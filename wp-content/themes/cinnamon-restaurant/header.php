<?php
/*
* @ Header
*/
?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta http-equiv="Content-Type" content="<?php bloginfo('html_type'); ?>; charset=<?php bloginfo('charset'); ?>" />
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
	<link rel="profile" href="http://gmpg.org/xfn/11" />
	<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />
	<?php
	wp_head();
	?>
</head>
<body <?php body_class(); ?>>
<?php
$site_in_maintenance = cinnamon_restaurant_maintenance_check();
do_action('cinnamon_restaurant_contextmenu_msg');
do_action('cinnamon_restaurant_preloader');
do_action('cinnamon_restaurant_page_necessities');
do_action('cinnamon_restaurant_social_screen');
if (is_page_template('template-blank.php')) {

	$site_layout_width='fullwidth';

} else {
	if (!$site_in_maintenance) {
		get_template_part('template-parts/menu/mobile','menu');
		//Header Navigation elements
		get_template_part('template-parts/header','navigation');
	}
}
echo '<div id="home" class="container-wrapper container-fullwidth">';
if (!cinnamon_restaurant_is_fullscreen_post()) {
	$header_menu_type = cinnamon_restaurant_get_option_data('header_menu_type');
	if (cinnamon_restaurant_is_in_demo()) {
		if ( false != cinnamon_restaurant_demo_get_data('menu_type') ) {
			$header_menu_type = cinnamon_restaurant_demo_get_data('menu_type');
		}
	}
	if ( cinnamon_restaurant_menu_is_vertical() ) {
		echo '<div class="vertical-menu-body-container">';
	}
}
if (!is_page_template('template-blank.php') && !cinnamon_restaurant_is_fullscreen_post() ) {
	if (!$site_in_maintenance) {
		get_template_part('template-parts/header','title');
	}
}
$post_type = get_post_type( get_the_ID() );
$custom = get_post_custom( get_the_ID() );
$mtheme_pagestyle='';
$header_menu_type = cinnamon_restaurant_get_option_data('header_menu_type');
if (cinnamon_restaurant_is_in_demo()) {
	if ( false != cinnamon_restaurant_demo_get_data('menu_type') ) {
		$header_menu_type = cinnamon_restaurant_demo_get_data('menu_type');
	}
}
if ($header_menu_type == "boxed-header-split" || $header_menu_type == "boxed-header-left") {
	echo '<div class="vertical-left-bar"></div>';
	echo '<div class="vertical-right-bar"></div>';
	$footer_info = stripslashes_deep( cinnamon_restaurant_get_option_data('footer_copyright') );
	echo '<div class="horizontal-bottom-bar">'.do_shortcode( $footer_info ).'</div>';
}
if (isset($custom['pagemeta_pagestyle'][0])) { $mtheme_pagestyle=$custom['pagemeta_pagestyle'][0]; }
if (!cinnamon_restaurant_is_fullscreen_post()) {
	echo '<div class="container clearfix">';
}
?>
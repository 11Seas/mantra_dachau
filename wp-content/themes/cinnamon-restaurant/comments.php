<?php
	if ( post_password_required() ) { ?>
		<p class="nocomments"><?php esc_html_e( 'This post is password protected. Enter the password to view any comments.', 'cinnamon-restaurant' ); ?></p>
	<?php
		return;
	}
?>

<!-- You can start editing here. -->
<?php if ( have_comments() ) : ?>
<div class="commentform-wrap entry-content">
	<h2 id="comments">
		<?php
			printf( _nx( 'One thought on &ldquo;%2$s&rdquo;', '%1$s thoughts on &ldquo;%2$s&rdquo;', get_comments_number(), 'comments title', 'cinnamon-restaurant' ),
				number_format_i18n( get_comments_number() ), get_the_title() );
		?>
	</h2>

	<div class="comment-nav">
		<div class="alignleft"><?php previous_comments_link() ?></div>
		<div class="alignright"><?php next_comments_link() ?></div>
	</div>

	<ol class="commentlist">
	<?php
	wp_list_comments( 'avatar_size=64' ); 
	?>
	</ol>

	<div class="comment-nav">
		<div class="alignleft"><?php previous_comments_link() ?></div>
		<div class="alignright"><?php next_comments_link() ?></div>
	</div>
</div>
 <?php else : // this is displayed if there are no comments so far ?>

	<?php if ( comments_open() ) : ?>
		<!-- If comments are open, but there are no comments. -->

	 <?php else : // comments are closed ?>
		<!-- If comments are closed. -->
		<p class="no-comments"><?php esc_html_e( 'Comments are closed.', 'cinnamon-restaurant' ); ?></p>
	<?php endif; ?>
<?php endif; ?>

<?php if ( comments_open() ) : ?>
	<?php
	$commenter = wp_get_current_commenter();
	$req       = get_option( 'require_name_email' );
	$aria_req  = ( $req ) ? " aria-required='true'" : '';
	$html_req  = ( $req ) ? " required='required'" : '';
	$html5     = ( 'html5' === current_theme_supports( 'html5', 'comment-form' ) ) ? 'html5' : 'xhtml';

	$fields = array();

	$fields['author'] = '<div id="comment-input"><input id="author" name="author" type="text" value="' . esc_attr( $commenter['comment_author'] ) . '" placeholder="' . esc_html__( 'Name (required)', 'cinnamon-restaurant' ) . '" size="30"' . $aria_req . $html_req . ' />';
	$fields['email']  = '<input id="email" name="email" ' . ( $html5 ? 'type="email"' : 'type="text"' ) . ' value="' . esc_attr(  $commenter['comment_author_email'] ) . '" placeholder="' . esc_html__( 'Email (required)', 'cinnamon-restaurant' ) . '" size="30" aria-describedby="email-notes"' . $aria_req . $html_req  . ' />';
	$fields['url']    = '<input id="url" name="url" ' . ( $html5 ? 'type="url"' : 'type="text"' ) . ' value="' . esc_attr( $commenter['comment_author_url'] ) . '" placeholder="' . esc_html__( 'Website', 'cinnamon-restaurant' ) . '" size="30" /></div>';

	if ( is_singular('mtheme_proofing') ) {
		$comments_args = array(
				'fields'               => apply_filters( 'comment_form_default_fields', $fields ),
				'comment_field'        => '<div id="comment-textarea"><label class="screen-reader-text" for="comment">' . esc_attr__( 'Message', 'cinnamon-restaurant' ) . '</label><textarea name="comment" id="comment" cols="45" rows="8" required="required" tabindex="0" class="textarea-comment" placeholder="' . esc_html__( 'Message...', 'cinnamon-restaurant' ) . '"></textarea></div>',
				'title_reply'          => esc_html__( 'Leave a message', 'cinnamon-restaurant' ),
				'title_reply_to'       => esc_html__( 'Leave a message', 'cinnamon-restaurant' ),
				'must_log_in'          => '<p class="must-log-in">' .  sprintf( esc_html__( 'You must be %slogged in%s to post a message.', 'cinnamon-restaurant' ), '<a href="' . wp_login_url( apply_filters( 'the_permalink', get_permalink() ) ) . '">', '</a>' ) . '</p>',
				'logged_in_as'         => '<p class="logged-in-as">' . sprintf( esc_html__( 'Logged in as %s. %sLog out &raquo;%s', 'cinnamon-restaurant' ), '<a href="' . admin_url( 'profile.php' ) . '">' . $user_identity . '</a>', '<a href="' . wp_logout_url( apply_filters( 'the_permalink', get_permalink() ) ) . '" title="' . esc_html__( 'Log out of this account', 'cinnamon-restaurant' ) . '">', '</a>' ) . '</p>',
				'comment_notes_before' => '',
				'id_submit'            => 'submit',
				'label_submit'         => esc_html__( 'Post Message', 'cinnamon-restaurant' ),
			);
	} else {
		$comments_args = array(
			'fields'               => apply_filters( 'comment_form_default_fields', $fields ),
			'comment_field'        => '<div id="comment-textarea"><label class="screen-reader-text" for="comment">' . esc_attr__( 'Comment', 'cinnamon-restaurant' ) . '</label><textarea name="comment" id="comment" cols="45" rows="8" required="required" tabindex="0" class="textarea-comment" placeholder="' . esc_html__( 'Comment...', 'cinnamon-restaurant' ) . '"></textarea></div>',
			'title_reply'          => esc_html__( 'Leave a comment', 'cinnamon-restaurant' ),
			'title_reply_to'       => esc_html__( 'Leave a comment', 'cinnamon-restaurant' ),
			'must_log_in'          => '<p class="must-log-in">' .  sprintf( esc_html__( 'You must be %slogged in%s to post a comment.', 'cinnamon-restaurant' ), '<a href="' . wp_login_url( apply_filters( 'the_permalink', get_permalink() ) ) . '">', '</a>' ) . '</p>',
			'logged_in_as'         => '<p class="logged-in-as">' . sprintf( esc_html__( 'Logged in as %s. %sLog out &raquo;%s', 'cinnamon-restaurant' ), '<a href="' . admin_url( 'profile.php' ) . '">' . $user_identity . '</a>', '<a href="' . wp_logout_url( apply_filters( 'the_permalink', get_permalink() ) ) . '" title="' . esc_html__( 'Log out of this account', 'cinnamon-restaurant' ) . '">', '</a>' ) . '</p>',
			'comment_notes_before' => '',
			'id_submit'            => 'submit',
			'label_submit'         => esc_html__( 'Post Comment', 'cinnamon-restaurant' ),
		);
	}

	?>

	<?php comment_form( $comments_args ); ?>

<?php endif; ?>
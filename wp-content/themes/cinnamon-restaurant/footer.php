<?php
/*
* Footer
*/
?>
<?php
$display_footer = true;
if (is_page_template('template-blank.php')) {
	$display_footer = false;
}
if ( post_password_required() ) {
	$display_footer = false;
}
if ( cinnamon_restaurant_is_fullscreen_post() ) {
	$display_footer = false;
}
if ( is_singular('mtheme_proofing') ) {
	$display_footer = true;
}
$site_in_maintenance = cinnamon_restaurant_maintenance_check();
if ($site_in_maintenance) {
	$display_footer = false;
}
if ( is_singular('mtheme_proofing') ) {
	$client_id = get_post_meta( get_the_id() , 'pagemeta_client_names', true);
	if ( isSet($client_id) ) {
		if ( post_password_required($client_id) ) {
			$display_footer = false;
		}
	}
}
if ( is_archive() ) {
	$display_footer = true;
}
if ( is_404() ) {
	$display_footer = false;
}
if ( is_search() ) {
	$display_footer = true;
}
?>
</div>
<?php
if ( $display_footer ) {
if (cinnamon_restaurant_get_option_data('footerwidget_status') ) {
?>
<footer class="footer-section clearfix">
<?php
if ( !wp_is_mobile() ) {
?>
<div id="goto-top"><i class="feather-icon-arrow-up"></i></div>
<?php
}
$display_footer_widgets = false;
if ( is_active_sidebar( 'footer_1' ) ) {
	$display_footer_widgets = true;
}
if ( is_active_sidebar( 'footer_2' ) ) {
	$display_footer_widgets = true;
}
if ( is_active_sidebar( 'footer_3' ) ) {
	$display_footer_widgets = true;
}
if ($display_footer_widgets) {
?>
	<div class="footer-container-wrap clearfix">
		<div class="footer-container clearfix">
			<div id="footer" class="sidebar widgetized clearfix">
			
				<?php if ( function_exists('dynamic_sidebar') ) { 
					echo '<div class="footer-column">';
					dynamic_sidebar("Footer Single Column 1");  
					echo '</div>';
					}
				?>
				<?php if ( function_exists('dynamic_sidebar') ) { 
					echo '<div class="footer-column">';
					dynamic_sidebar("Footer Single Column 2");  
					echo '</div>';
					}
				?>
				<?php if ( function_exists('dynamic_sidebar') ) { 
					echo '<div class="footer-column">';
					dynamic_sidebar("Footer Single Column 3"); 
					echo '</div>';
					}
				?>
			</div>	
		</div>
	</div>
<?php
}
?>
	<div id="copyright" class="footer-container">
	<?php
	$footer_info = stripslashes_deep( cinnamon_restaurant_get_option_data('footer_copyright') );
	echo do_shortcode( $footer_info );
	?>
	</div>
</footer>
<?php
}
}
?>
<?php
if (!cinnamon_restaurant_is_fullscreen_post()) {
	$header_menu_type = cinnamon_restaurant_get_option_data('header_menu_type');
	if (cinnamon_restaurant_is_in_demo()) {
		if ( false != cinnamon_restaurant_demo_get_data('menu_type') ) {
			$header_menu_type = cinnamon_restaurant_demo_get_data('menu_type');
		}
	}
	if ( cinnamon_restaurant_menu_is_vertical() ) {
		echo '</div>';
	}
	echo '</div>';
}
?>
<?php
do_action ( 'cinnamon_restaurant_starting_footer' );
wp_footer();
?>
</body>
</html>
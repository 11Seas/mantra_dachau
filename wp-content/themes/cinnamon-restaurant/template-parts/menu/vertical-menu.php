<div class="vertical-menu clearfix">
	<div class="vertical-logo-wrap">
			<?php
			$vmain_logo=cinnamon_restaurant_get_option_data('vmain_logo');
			$home_url_path = home_url('/');

			$menu_logo = '';

			if ( $vmain_logo <> "" ) {
				$menu_logo .= '<img class="vertical-logoimage" src="'.esc_url($vmain_logo).'" alt="logo" />';
			} else {
				$menu_logo .= '<img class="vertical-logoimage" src="'.esc_url(get_template_directory_uri() . '/images/logo_bright_v.png').'" alt="logo" />';
			}
			echo '<a href="'.esc_url($home_url_path).'">' . $menu_logo . '</a>';
			?>
	</div>
	<?php
	// WPML
	$wpml_lang_selector_enable= cinnamon_restaurant_get_option_data('wpml_lang_selector_enable');
	if ($wpml_lang_selector_enable) {
	?>
	<div class="mobile-wpml-lang-selector-wrap">
		<div class="flags_language_selector"><?php cinnamon_restaurant_language_selector_flags(); ?></div >
	</div>
	<?php
	}
	?>
	<nav>
	<?php
	$custom_menu_call = '';
	if ( is_singular() ) {
		$user_choice_of_menu = get_post_meta( get_the_id() , 'pagemeta_menu_choice', true);
		if ( cinnamon_restaurant_page_is_woo_shop() ) {
			$woo_shop_post_id = get_option( 'woocommerce_shop_page_id' );
			$user_choice_of_menu = get_post_meta( $woo_shop_post_id , 'pagemeta_menu_choice', true);
		}
		if ( isSet($user_choice_of_menu) && $user_choice_of_menu <> "default") {
			$custom_menu_call = $user_choice_of_menu;
		}
	}
	// Responsive menu conversion to drop down list
	if ( function_exists('wp_nav_menu') ) { 
		wp_nav_menu( array(
		 'container' =>false,
		 'theme_location' => 'main_menu',
		 'menu' => $custom_menu_call,
		 'menu_class' => 'mtree',
		 'echo' => true,
		 'before' => '',
		 'after' => '',
		 'link_before' => '',
		 'link_after' => '',
		 'depth' => 0,
		 'fallback_cb' => 'mtheme_nav_fallback'
		 )
		);
	}
	?>
	</nav>
	<div class="vertical-footer-wrap">
		<?php
		if ( is_active_sidebar( 'social_header' ) ) {
		?>
		<div class="fullscreen-footer-social">
			<div class="login-socials-wrap clearfix">
			<?php
			dynamic_sidebar('social_header');
			?>
			</div>
		</div>
		<?php
		}
		?>
	</div>
</div>
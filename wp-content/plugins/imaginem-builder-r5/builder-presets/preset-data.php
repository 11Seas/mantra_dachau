<?php
$presets = array();
$presets[] = array( "name"			=> __("Home Food Blog", "mthemelocal" ),
					"slug"			=> "home-blog");
$presets[] = array( "name"			=> __("Homepage I", "mthemelocal" ),
					"slug"			=> "homepage-i");
$presets[] = array( "name"			=> __("Homepage II", "mthemelocal" ),
					"slug"			=> "homepage-ii");
$presets[] = array( "name"			=> __("Homepage III", "mthemelocal" ),
					"slug"			=> "homepage-iii");
$presets[] = array( "name"			=> __("Homepage Video", "mthemelocal" ),
					"slug"			=> "homepage-video");
$presets[] = array( "name"			=> __("OnePage", "mthemelocal" ),
					"slug"			=> "onepage");
$presets[] = array( "name"			=> __("About Us", "mthemelocal" ),
					"slug"			=> "about-us");
$presets[] = array( "name"			=> __("Contact us", "mthemelocal" ),
					"slug"			=> "contact-us");
$presets[] = array( "name"			=> __("Contact Multi Map", "mthemelocal" ),
					"slug"			=> "contact-multi");
$presets[] = array( "name"			=> __("Our Services", "mthemelocal" ),
					"slug"			=> "our-services");
$presets[] = array( "name"			=> __("Our Team", "mthemelocal" ),
					"slug"			=> "our-team");
$presets[] = array( "name"			=> __("Reservation", "mthemelocal" ),
					"slug"			=> "reservation");
$presets[] = array( "name"			=> __("Reservation II", "mthemelocal" ),
					"slug"			=> "reservation-ii");
?>
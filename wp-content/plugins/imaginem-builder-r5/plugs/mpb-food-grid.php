<?php
/** Food Grid **/
if(!class_exists('em_food_grid')) {
		class em_food_grid extends AQ_Block {

		protected $the_options;
		protected $food_tax;

		function init_reg() {
			$the_list = get_terms('foodcategory');
			//print_r($the_list);
			// Pull all the Food Categories into an array
			if ($the_list) {
				$food_categories=array();
				//$food_categories[0]="All the items";
				foreach($the_list as $key => $list) {
					if (isSet($list->slug)) {
						$food_categories[$list->slug] = $list->name;
					}
				}
			} else {
				$food_categories[0]="Food Categories not found.";
			}
			$this->food_store($food_categories);

		}

		function food_store($food_categories) {
			$this->the_options['category_list'] = $food_categories;
		}

		//set and create block
		function __construct() {
			$block_options = array(
				'pb_block_icon' => 'simpleicon-grid',
				'pb_block_icon_color' => '#836953',
				'name' => __('Food Grid','mthemelocal'),
				'size' => 'span12',
				'tab' => __('Food','mthemelocal'),
				'desc' => __('Generate a Food Grid','mthemelocal')
			);
			add_action('init', array(&$this, 'init_reg'));

			$mtheme_shortcodes['foodgrid'] = array(
				'no_preview' => true,
				'shortcode_desc' => __('A Grid based Grid of food items.', 'mthemelocal'),
				'params' => array(
					'foodtype_slugs' => array(
						'type' => 'category_list',
						'std' => '',
						'label' => __('Choose Food types to list', 'mthemelocal'),
						'desc' => __('Leave blank to list all. Enter comma seperated food type categories. eg. mains,dessert,dinner ', 'mthemelocal'),
						'options' => ''
					),
					'columns' => array(
						'type' => 'select',
						'label' => __('Columns', 'mthemelocal'),
						'desc' => __('No. of Columns', 'mthemelocal'),
						'options' => array(
							'4' => '4',
							'3' => '3',
							'2' => '2',
							'1' => '1',
						)
					),
					'boxtitle' => array(
						'type' => 'select',
						'label' => __('Box title', 'mthemelocal'),
						'desc' => __('Box title', 'mthemelocal'),
						'options' => array(
							'false' => __('No','mthemelocal'),
							'true' => __('Yes','mthemelocal')
						)
					),
					'gutter' => array(
						'type' => 'select',
						'label' => __('Gutter Space', 'mthemelocal'),
						'desc' => __('Gutter Space', 'mthemelocal'),
						'options' => array(
							'spaced' => __('Spaced','mthemelocal'),
							'narrow-spaced' => __('Narrow Spaced','mthemelocal'),
							'nospace' => __('No Space','mthemelocal')
						)
					),
					'limit' => array(
						'std' => '-1',
						'type' => 'text',
						'label' => __('Limit. -1 for unlimited', 'mthemelocal'),
						'desc' => __('Limit items. -1 for unlimited', 'mthemelocal'),
					)
				),
				'shortcode' => '[foodgrid title="{{title}}" gutter="{{gutter}}" boxtitle="{{boxtitle}}" padding="{{padding}}" background_color="{{background_color}}" button_url="{{button_url}}" button_text="{{button_text}}" foodtype_slugs="{{foodtype_slugs}}" item_image="{{item_image}}" columns={{columns}} limit="{{limit}}"]{{content}}[/foodgrid]',
				'popup_title' => __('Add Food Grid', 'mthemelocal')
			);
			$this->the_options = $mtheme_shortcodes['foodgrid'];

			//create the block
			parent::__construct('em_food_grid', $block_options);
			// Any script registers need to uncomment following line
			//add_action('mtheme_aq-page-builder-admin-enqueue', array($this, 'admin_enqueue_Block'));
			
		}

		function form($instance) {
			$instance = wp_parse_args($instance);

			echo mtheme_generate_builder_form($this->the_options,$instance);
			//extract($instance);
		}

		function block($instance) {

			wp_enqueue_script ('isotope');
			
			extract($instance);
			$shortcode = mtheme_dispay_build($this->the_options,$block_id,$instance);

			echo do_shortcode($shortcode);
			
		}
		function mtheme_enqueue_em_food_grid(){
			//Any script registers go here
		}

	}
}
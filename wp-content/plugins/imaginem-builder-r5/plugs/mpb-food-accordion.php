<?php
/** Food Grid **/
if(!class_exists('em_food_tabaccordion')) {
		class em_food_tabaccordion extends AQ_Block {

		protected $the_options;
		protected $food_tax;

		function init_reg() {
			$the_list = get_terms('foodcategory');
			//print_r($the_list);
			// Pull all the Food Categories into an array
			if ($the_list) {
				$food_categories=array();
				//$food_categories[0]="All the items";
				foreach($the_list as $key => $list) {
					if (isSet($list->slug)) {
						$food_categories[$list->slug] = $list->name;
					}
				}
			} else {
				$food_categories[0]="Food Categories not found.";
			}
			$this->food_store($food_categories);

		}

		function food_store($food_categories) {
			$this->the_options['category_list'] = $food_categories;
		}

		//set and create block
		function __construct() {
			$block_options = array(
				'pb_block_icon' => 'simpleicon-grid',
				'pb_block_icon_color' => '#836953',
				'name' => __('Food Accordion','mthemelocal'),
				'size' => 'span12',
				'tab' => __('Food','mthemelocal'),
				'desc' => __('Generate Food Accordion','mthemelocal')
			);
			add_action('init', array(&$this, 'init_reg'));

			$mtheme_shortcodes['foodaccordion'] = array(
				'no_preview' => true,
				'shortcode_desc' => __('Accordion based list of food items.', 'mthemelocal'),
				'params' => array(
					'foodtype_slugs' => array(
						'type' => 'category_list',
						'std' => '',
						'label' => __('Choose Food types to list', 'mthemelocal'),
						'desc' => __('Leave blank to list all. Enter comma seperated food type categories. eg. mains,dessert,dinner ', 'mthemelocal'),
						'options' => ''
					),
			        'title' => array(
			            'std' => '',
			            'type' => 'text',
			            'label' => __('Title', 'mthemelocal'),
			            'desc' => __('Title for list', 'mthemelocal'),
			        ),
			        'content' => array(
			            'std' => '',
			            'textformat' => 'richtext',
			            'type' => 'editor',
			            'label' => __('Text', 'mthemelocal'),
			            'desc' => __('Text', 'mthemelocal'),
			        ),
					'item_image' => array(
						'type' => 'select',
						'label' => __('Item Image', 'mthemelocal'),
						'desc' => __('Item Image', 'mthemelocal'),
						'options' => array(
							'no' => 'No',
							'yes' => 'Yes'
						)
					),
					'limit' => array(
						'std' => '-1',
						'type' => 'text',
						'label' => __('Limit. -1 for unlimited', 'mthemelocal'),
						'desc' => __('Limit items. -1 for unlimited', 'mthemelocal'),
					)
				),
				'shortcode' => '[foodaccordion title="{{title}}" foodtype_slugs="{{foodtype_slugs}}" item_image="{{item_image}}" limit="{{limit}}"]{{content}}[/foodaccordion]',
				'popup_title' => __('Insert Food Accordion', 'mthemelocal')
			);
			$this->the_options = $mtheme_shortcodes['foodaccordion'];

			//create the block
			parent::__construct('em_food_tabaccordion', $block_options);
			// Any script registers need to uncomment following line
			//add_action('mtheme_aq-page-builder-admin-enqueue', array($this, 'admin_enqueue_Block'));
			
		}

		function form($instance) {
			$instance = wp_parse_args($instance);

			echo mtheme_generate_builder_form($this->the_options,$instance);
			//extract($instance);
		}

		function block($instance) {

		    wp_enqueue_script('jquery-ui-core');
		    wp_enqueue_script('jquery-ui-tabs');
		    wp_enqueue_script('jquery-ui-accordion');

			extract($instance);
			$shortcode = mtheme_dispay_build($this->the_options,$block_id,$instance);

			echo do_shortcode($shortcode);
			
		}
		function mtheme_enqueue_em_food_tabaccordion(){
			//Any script registers go here
		}

	}
}